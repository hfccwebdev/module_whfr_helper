<?php

namespace Drupal\whfr_helper\Commands;

use Drush\Commands\DrushCommands;
use Drupal\whfr_helper\WhfrContentImportService;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WhfrImportCommands extends DrushCommands {

  /**
   * Stores the content import service.
   *
   * @var \Drupal\whfr_helper\WhfrContentImportService
   */
  protected $import;

  /**
   * Class constructor.
   *
   * @param \Drupal\whfr_helper\WhfrContentImportService $import
   *   The content import service.
   */
  public function __construct(WhfrContentImportService $import) {
    $this->import = $import;
  }

  /**
   * Import legacy content.
   *
   * @param string $type
   *   The content type to import.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option limit
   *   Number of entries to import.
   * @usage drush whfr:content-import podcast --limit=500
   *   Import content of the specified type.
   *   (gallery library photo podcast story comment)
   * @validate-module-enabled whfr_helper
   *
   * @command whfr:content-import
   * @aliases wcim
   */
  public function importContent(string $type, array $options = ['limit' => 0]) {
    $this->import->import($type, $options);
  }

}
