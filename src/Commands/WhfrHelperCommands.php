<?php

namespace Drupal\whfr_helper\Commands;

use Drupal\whfr_helper\WhfrHelperService;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WhfrHelperCommands extends DrushCommands {

  /**
   * Stores the helper service.
   *
   * @var \Drupal\whfr_helper\WhfrHelperService
   */
  protected $helper;

  /**
   * Class constructor.
   *
   * @param \Drupal\whfr_helper\WhfrHelperService $helper
   *   The WHFR helper service.
   */
  public function __construct(WhfrHelperService $helper) {
    $this->helper = $helper;
  }

}
