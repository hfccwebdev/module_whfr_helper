<?php

namespace Drupal\whfr_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Defines the WHFR Cover Art Service.
 */
class CoverArtService {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Module configuration settings name.
   */
  const MODULE_SETTINGS = 'whfr_helper.settings';

  /**
   * MusicBrainz Releases URL.
   */
  const MB_RELEASE_URL = 'https://musicbrainz.org/ws/2/release';

  /**
   * Cover Art Archive release URL.
   */
  const COVER_ART_URL = 'https://coverartarchive.org/release/';

  /**
   * Spotify API token URL.
   */
  const SPOTIFY_TOKEN_URL = 'https://accounts.spotify.com/api/token';

  /**
   * Spotify API search URL.
   */
  const SPOTIFY_SEARCH_URL = 'https://api.spotify.com/v1/search';

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The Cache Backend service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle HTTP Client.
   */
  public function __construct(
    private readonly CacheBackendInterface $cache,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly TimeInterface $time,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ClientInterface $httpClient
  ) {
  }

  /**
   * Get cover art.
   */
  public function getCoverArt(string $album = NULL, string $artist, string $song, string $size = NULL): ?string {
    if (empty($album)) {
      $image = $this->getSpotify($song, $artist);
      return $image;
    }

    $releases = $this->getAlbumReleases($album, $artist);
    if (empty($releases)) {
      $image = $this->getSpotify($song, $artist, $album);
      return $image;
    }

    foreach ($releases as $release) {
      if ($cover = $this->getReleaseCoverArt($release->id)) {
        $image = reset($cover->images);
        if ($size && isset($image->thumbnails->{$size})) {
          return $image->thumbnails->{$size};
        }
        return $image->image;
      }
    }
    return NULL;
  }

  /**
   * Get Music Brainz release.
   */
  public function getAlbumReleases(string $album, string $artist): array {

    $cid = "mbrelease_{$album}_{$artist}";
    if ($result = $this->cache->get($cid)) {
      return $result->data;
    }

    $response = $this->httpClient->request('GET', self::MB_RELEASE_URL, [
      RequestOptions::QUERY  => [
        'query' => $album,
      ],
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
      ],
      RequestOptions::TIMEOUT => 10,
      RequestOptions::HTTP_ERRORS => FALSE,
    ]);
    if ($response->getStatusCode() !== 200) {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return [];
    }

    $body = $response->getBody();
    $result = json_decode($body);

    $releases = [];

    foreach ($result->releases as $release) {
      if ($this->artistMatch($release->{"artist-credit"}, $artist)) {
        $releases[] = $release;
      }
    }

    $this->cache->set($cid, $releases);
    return $releases;
  }

  /**
   * Get cover art from mbid.
   */
  public function getReleaseCoverArt(string $mbid) {

    $cid = "coverart_{$mbid}";
    if ($result = $this->cache->get($cid)) {
      return $result->data;
    }

    $url = self::COVER_ART_URL . $mbid;
    $response = $this->httpClient->request('GET', $url, [
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
      ],
      RequestOptions::TIMEOUT => 10,
      RequestOptions::HTTP_ERRORS => FALSE,
    ]);
    if ($response->getStatusCode() == 404) {
      return NULL;
    }
    if ($response->getStatusCode() !== 200) {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return NULL;
    }

    $body = $response->getBody();
    $data = json_decode($body);
    $this->cache->set($cid, $data);
    return $data;
  }

  /**
   * Try to match artist info.
   */
  private function artistMatch($credits, $artist): bool {
    foreach ($credits as $credit) {
      if (mb_strtolower($credit->name) == mb_strtolower($artist)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check Spotify for cover art.
   */
  public function getSpotify(string $song, string $artist, string $album = NULL) {
    $song = $this->stringClean($song);
    $artist_search = 'artist:' . $this->stringClean($artist);
    if (!empty($album)) {
      $album_search = 'album:' . $this->stringClean($album);
    }
    else {
      $album_search = '';
    }
    $query = implode('%20', array_filter([$song, $artist_search, $album_search]));

    if (empty($query)) {
      return NULL;
    }

    $cid = "spotify_{$query}";
    if ($result = $this->cache->get($cid)) {
      return $result->data;
    }

    $token = $this->getSpotifyToken();
    $auth = "Bearer {$token}";

    $response = $this->httpClient->request('GET', self::SPOTIFY_SEARCH_URL, [
      RequestOptions::QUERY  => [
        'q' => $query,
        'type' => 'track',
        'limit' => 1,
      ],
      RequestOptions::HEADERS => [
        'Authorization' => $auth,
      ],
    ]);
    if ($response->getStatusCode() !== 200) {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return [];
    }

    $body = $response->getBody();
    $result = json_decode($body);

    if (!isset($result->tracks->items[0]->album->images[1]->url)) {
      $this->getLogger('whfr_content_import')->warning(
        'Bad Spotify Search Query: @query',
        [
          '@query' => $query,
        ]
      );
      return NULL;
    }

    $cid = "spotify_{$query}";
    $image = $result->tracks->items[0]->album->images[1]->url;
    $this->cache->set($cid, $image);

    return $image;
  }

  /**
   * Get an API token from Spotify.
   */
  public function getSpotifyToken() {
    if ($result = $this->cache->get('spotify_token')) {
      $token = $result->data;
      return $token;
    }

    $client_id = $this->getSettings('spotify_client_id');
    $client_secret = $this->getSettings('spotify_client_secret');

    $response = $this->httpClient->request('POST', self::SPOTIFY_TOKEN_URL, [
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      RequestOptions::FORM_PARAMS => [
        'grant_type' => 'client_credentials',
        'client_id' => $client_id,
        'client_secret' => $client_secret,
      ],
    ]);

    if ($response->getStatusCode() !== 200) {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $token_response->getStatusCode(),
          '@message' => $token_response->getReasonPhrase(),
        ]
      );
      return [];
    }

    $contents = $response->getBody()->getContents();
    if (empty($contents)) {
      return NULL;
    }
    $contents = json_decode($contents);

    $token = $contents->access_token;
    $expiration = $contents->expires_in - 120;
    $expire = $this->time->getRequestTime() + $expiration;
    $cid = 'spotify_token';

    $this->cache->set($cid, $token, $expire);

    return $token ?? NULL;
  }

  /**
   * Strip strings of special characters.
   *
   * @param string $string
   *   The string to clean.
   */
  private function stringClean($string) {
    $cleaned = preg_replace('/[^A-Za-z0-9 ]/', '', $string);
    $replaced = str_replace(' ', '%20', $cleaned);
    return $replaced;
  }

  /**
   * Get import settings.
   *
   * @param string $key
   *   The settings key to retrieve.
   */
  private function getSettings(string $key) {
    $settings = $this->configFactory->get(static::MODULE_SETTINGS);
    return $settings->get($key) ?? NULL;
  }

}
