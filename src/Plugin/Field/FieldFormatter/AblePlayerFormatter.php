<?php

namespace Drupal\whfr_helper\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterBase;

/**
 * Plugin implementation of the Able Player formatter.
 *
 * @FieldFormatter(
 *   id = "able_player_formatter",
 *   label = @Translation("Able Player"),
 *   description = @Translation("Display the file using Able Player."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class AblePlayerFormatter extends FileMediaFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'audio';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $build = [
      '#theme' => 'whfr_audio_player',
      'source' => [],
    ];

    foreach ($elements as &$element) {
      foreach ($element['#files'] as $attachment) {
        $build['source'][] = $attachment['file']->createFileUrl();
      }
    }

    return $build;
  }

}
