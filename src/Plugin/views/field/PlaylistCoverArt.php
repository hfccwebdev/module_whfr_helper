<?php

namespace Drupal\whfr_helper\Plugin\views\field;

use Drupal\Component\Render\MarkupTrait;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\whfr_helper\CoverArtService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to display credentials information.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("playlist_coverart")
 */
class PlaylistCoverArt extends FieldPluginBase {

  use MarkupTrait;

  /**
   * Stores the Cover Art service.
   *
   * @var \Drupal\whfr_helper\CoverArtService
   */
  private $coverArtService;

  /**
   * @{inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('whfr_cover_art')
    );
  }

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\whfr_helper\CoverArtService $coverArtService
   *   The Cover Art service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CoverArtService $coverArtService
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->coverArtService = $coverArtService;
    $this->is_handler = TRUE;
  }

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
    // We will do our own below.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    // @todo add a configuration form to choose image sizes.
    if ($playlist = $values->_entity) {
      $image = $this->coverArtService->getCoverArt(
        $playlist->album->value,
        $playlist->artist->value,
        $playlist->song->value,
        '250'
      );
      return $image;
    }
  }

}
