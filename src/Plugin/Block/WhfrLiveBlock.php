<?php

namespace Drupal\whfr_helper\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The WHFR Livestream Block.
 *
 * @Block(
 *  id = "whfr_live_block",
 *  admin_label = @Translation("WHFR live stream player"),
 * )
 */
class WhfrLiveBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['whfr_live_stream'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['whfr_live_stream']['whfr_live_stream_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WHFR live stream URL'),
      '#default_value' => $config['whfr_live_stream_url'],
      '#description' => $this->t('Enter the URL for the live broadcast stream.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $this->configuration['whfr_live_stream_url'] = $values['whfr_live_stream']['whfr_live_stream_url'];

    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];

    $build = [
      '#theme' => 'whfr_audio_player',
      'source' => [$this->getConfiguration()['whfr_live_stream_url']],
    ];

    return $build;
  }

}
