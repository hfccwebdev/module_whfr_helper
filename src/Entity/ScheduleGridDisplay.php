<?php

namespace Drupal\whfr_helper\Entity;

/**
 * Defines the Schedule Grid Display entity type.
 *
 * This is not a normal Drupal entity!
 *
 * This entity contains the logic needed by the twig template
 * to render a schedule array.
 */
class ScheduleGridDisplay {

  /**
   * Stores an array of WHFR programs.
   *
   * @var array
   */
  private $programs;

  /**
   * Stores the earliest timeslot in the grid.
   *
   * @var int
   */
  private $first = 0;

  /**
   * Create an new instance of this class.
   *
   * @param object[] $programs
   *   An array of WHFR Program entities.
   */
  public static function create(array $programs) {
    return new static($programs);
  }

  /**
   * Class constructor.
   *
   * @param object[] $programs
   *   An array of WHFR Program entities.
   */
  public function __construct(array $programs) {
    $this->programs = $programs;
  }

  /**
   * Get data from the entity.
   *
   * This method is required to work around TwigSandboxPolicy.
   *
   * @param string $method
   *   Defines the internal method to query.
   * @param string $opts
   *   Optional arguments to pass as a string.
   */
  public function get(string $method, string $opts = NULL) {
    if (method_exists($this, $method)) {
      return $this->$method($opts);
    }
  }

  /**
   * Get the earliest timeslot in the grid.
   *
   * @return string
   *   Because apparently twig doesn't like integers.
   */
  private function first(): string {
    // Make sure first has been calculated!
    $this->grid();
    return $this->first / 100;
  }

  /**
   * Determine if timeslot is blank.
   *
   * The slot will not be blank if it is between
   * the start and end times of another show.
   *
   * @param string $opts
   *   A comma-delimited string of day,time to check.
   *
   * @return string|null
   *   Return a string if the blank cell _should_ be rendered.
   */
  private function blank(string $opts): ?string {
    $blank = NULL;
    $first = $this->first;

    [$day, $now] = explode(',', $opts);

    foreach ($this->grid()[$day] as $starttime => $show) {
      if ($now < $starttime) {
        if ($now == $first) {
          $blank = ($starttime - $first) / 100;
        }
        break;
      }
      elseif ($starttime < $now && $now < $show['endhours']) {
        break;
      }
      elseif ($now == $show['endhours']) {
        $first = $show['endhours'];
        $blank = (2400 - $now) / 100;
      }
    }

    return $blank;
  }

  /**
   * Display the formatted time.
   *
   * @param int $time
   *   The incoming unformatted time in HHMM.
   *
   * @return string
   *   The formatted time.
   *
   * @todo Twig is not cooperating here.
   *   Try again with Twig 3 in Drupal 10,
   *   because this sucks.
   */
  private function showtime(int $time): string {

    if ($time == 0) {
      return '12:00am';
    }

    $hours = round($time / 100);
    $minutes = str_pad($time % 100, 2, '0', STR_PAD_LEFT);

    if ($hours == 12) {
      $a = 'pm';
    }
    elseif ($hours >= 13) {
      $hours = $hours - 12;
      $a = 'pm';
    }
    else {
      $a = 'am';
    }

    return "{$hours}:{$minutes}{$a}";
  }

  /**
   * Build the schedule grid array.
   */
  private function grid(): array {
    $grid = &drupal_static(__FUNCTION__);
    if (!isset($grid)) {

      // Initialize seven day grid array.
      $grid = [];
      for ($i = 0; $i < 7; $i++) {
        $grid[$i] = [];
      }

      $this->first = 2400;

      // Add scheduled items to grid.
      foreach ($this->programs as $program) {
        foreach ($program->field_program_schedule->getValue() as $schedule) {
          if ($schedule['starthours'] < $this->first) {
            $this->first = $schedule['starthours'];
          }
          $grid[$schedule['day']][$schedule['starthours']] = [
            'endhours' => ($schedule['endhours'] === 0) ? 2400 : $schedule['endhours'],
            'link' => $program->toLink(),
          ];
        }
      }

      // Sort the days by start time.
      foreach (array_keys($grid) as $key) {
        ksort($grid[$key]);
      }
    }
    return $grid;
  }

  /**
   * Get the current show for a specified day and time.
   */
  private function current(string $opts): array {

    [$day, $now] = explode(',', $opts);

    $keys = array_keys($this->grid()[$day]);
    $first = reset($keys);
    $last = '0000';

    if ($now < $first) {
      return [
        'starthours' => '0000',
        'endhours' => $first,
        'link' => 'Classical 24',
        'startpretty' => 'Now',
        'endpretty' => $this->showtime($first),
      ];
    }

    foreach ($this->grid()[$day] as $key => $show) {
      if ($key <= $now && $now <= $show['endhours']) {
        $show['starthours'] = $key;
        $show['startpretty'] = 'Now';
        $show['endpretty'] = $this->showtime($show['endhours']);
        return $show;
      }
      elseif ($now < $key) {
        return [
          'starthours' => $last,
          'endhours' => $key,
          'link' => 'Classical 24',
          'startpretty' => 'Now',
          'endpretty' => $this->showtime($key),
        ];
      }
      else {
        $last = $show['endhours'];
      }
    }

    $next = $this->next($opts);
    return [
      'starthours' => $last,
      'endhours' => $next['endhours'],
      'link' => 'Classical 24',
      'startpretty' => 'Now',
      'endpretty' => $next['endpretty'],
    ];
  }

  /**
   * Returns the next scheduled program from a given day and time.
   */
  private function next(string $opts): array {
    [$day, $now] = explode(',', $opts);

    foreach ($this->grid()[$day] as $key => $show) {
      if ($now <= $key) {
        $show['starthours'] = $key;
        $show['weekday'] = $day;
        $show['startpretty'] = $this->showtime($key);
        $show['endpretty'] = $this->showtime($show['endhours']);
        return $show;
      }
    }

    $day = ($day + 1) % 7;
    return $this->next("$day,0000");
  }

}
