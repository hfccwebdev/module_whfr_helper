<?php

namespace Drupal\whfr_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\whfr_playlist\WhfrPlaylistImportService;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Defines the WHFR Playlist Import Service.
 */
class WhfrContentImportService {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Module configuration settings name.
   */
  const MODULE_SETTINGS = 'whfr_helper.settings';

  /**
   * Import configuration settings name.
   *
   * @var string
   */
  const IMPORT_SETTINGS = 'whfr_helper.import_settings';

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Stores the WHFR Playlist Import service.
   *
   * @var \Drupal\whfr_playlist\WhfrPlaylistImportService
   */
  private $playlistImport;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP Client.
   * @param \Drupal\whfr_playlist\WhfrPlaylistImportService $whfr_playlist_import
   *   The WHFR Playlist Import service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TimeInterface $time,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
    WhfrPlaylistImportService $whfr_playlist_import
  ) {
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->playlistImport = $whfr_playlist_import;
  }

  /**
   * Import legacy content.
   *
   * @param string $type
   *   The content type to import.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   */
  public function import(string $type, array $options): void {

    if ($type == 'comment') {
      $this->importComment($options);
      return;
    }

    // Exit unless the specified content type is explicitly allowed here.
    if (!in_array($type, ['gallery', 'library', 'photo', 'podcast', 'story'])) {
      $this->getLogger('whfr_content_import')->error(
        'Invalid content type @type requested for import.',
        ['@type' => $type]
      );
      return;
    }

    // Limit the number of items to import.
    $limit = intval($options['limit'] ?? 0);
    if ($limit === 0) {
      $limit = $this->getSettings('content_import_limit') ?? 500;
    }

    // Get an index of nodes to import.
    $index = $this->getIndex($type);
    $count = 0;

    foreach ($index as $item) {

      if ($this->checkExisting($item->created)) {
        continue;
      }

      if ($node = $this->getContent($item->nid)) {
        $importMethod = 'import' . ucfirst($type);
        if (method_exists(__CLASS__, $importMethod)) {
          $this->$importMethod($node);
          // Incrementing the counter
          // without checking the result
          // might be dumb.
          $count++;
        }
      }

      if ($count >= $limit) {
        break;
      }
    }

    $this->messenger()->addMessage($this->t(
      'Imported @count items.',
      ['@count' => $count]
    ));

  }

  /**
   * Import a gallery node.
   */
  private function importGallery($source): int {

    $values = $this->getBaseValues($source);
    $node = $this->entityTypeManager->getStorage('node')->create($values);
    $node->title->setValue($source->title);
    $node->body->setValue([
      'value' => $source->body,
      'format' => 'basic_html',
    ]);
    $node->path->setValue('/' . $source->path);
    $node->field_legacy_nid->setValue($source->nid);
    $node->save();
    return $node->id() ?? 0;
  }

  /**
   * Import a library node.
   */
  private function importLibrary($source): int {

    $values = $this->getBaseValues($source);
    $node = $this->entityTypeManager->getStorage('node')->create($values);
    $node->title->setValue($source->title);
    $node->body->setValue([
      'value' => $source->body,
      'format' => 'basic_html',
    ]);
    $node->field_album->setValue($source->field_library_album[0]->value);
    $node->field_artist->setValue($source->field_library_artist[0]->value);
    $node->field_label->setValue($source->field_library_label[0]->value);
    $node->field_date_added->setValue($source->field_library_date[0]->value);
    $node->field_date_added->setValue(mb_substr($source->field_library_date[0]->value, 0, 10));
    $node->path->setValue('/' . $source->path);
    $node->save();
    return $node->id() ?? 0;
  }

  /**
   * Import a photo node.
   */
  private function importPhoto($source): int {

    $values = $this->getBaseValues($source);
    $node = $this->entityTypeManager->getStorage('node')->create($values);
    $node->title->setValue($source->title);

    $gid = $source->field_gallery[0]->nid;
    $gallery = $this->checkGallery($gid);

    if (empty($gallery)) {
      $this->getLogger('whfr_import')->warning(
        "Gallery @gid not found for photo @title",
        ['@tid' => $gid, '@title' => $source->title]
      );
    }
    else {
      $node->field_gallery->target_id = $gallery;
    }

    $year = $this->dateFormatter->format($source->created, 'custom', 'Y');
    if ($media = $this->getMedia('image', $source->field_image[0], "photos/{$year}")) {
      $node->field_image->target_id = $media->id();
      $node->field_image->entity = $media;
      $node->save();
      return $node->id() ?? 0;
    }

    return 0;
  }

  /**
   * Import a podcast node.
   */
  private function importPodcast($source): int {

    if (empty($source->field_audio_file[0])) {
      $this->getLogger('whfr_import')->warning(
        "No file attachment found for podcast @url",
        ['@url' => $this->getSettings('origin') . '/node/' . $source->nid]
      );
      return 0;
    }

    $tid = $source->field_program[0]->nid;
    $program = $this->playlistImport->checkProgram($tid);
    if (empty($program)) {
      $this->getLogger('whfr_import')->warning(
        "Program @tid not found for podcast @title",
        ['@tid' => $tid, '@title' => $source->title]
      );
      return 0;
    }

    $values = $this->getBaseValues($source);
    $node = $this->entityTypeManager->getStorage('node')->create($values);
    $node->title->setValue($source->title);
    $node->body->setValue([
      'value' => $source->body,
      'format' => 'basic_html',
    ]);
    $node->field_program->target_id = $program;

    if ($media = $this->getMedia('audio', $source->field_audio_file[0], 'podcasts')) {
      $node->field_podcast_file->target_id = $media->id();
      $node->field_podcast_file->entity = $media;
      $node->save();
      return $node->id() ?? 0;
    }
    else {
      $this->getLogger('whfr_import')->warning(
        "Could note retrieve file @filename for podcast @url",
        [
          '@filename' => $source->field_audio_file[0]->filename,
          '@url' => $this->getSettings('origin') . '/node/' . $source->nid,
        ]
      );
    }

    return 0;
  }

  /**
   * Import a story node.
   */
  private function importStory($source): int {

    // We need to alter the content type for this one.
    $source->type = 'article';
    $values = $this->getBaseValues($source);
    $node = $this->entityTypeManager->getStorage('node')->create($values);
    $node->title->setValue($source->title);
    $node->body->setValue([
      'value' => $source->body,
      'format' => 'basic_html',
    ]);
    $node->path->setValue('/' . $source->path);
    $node->save();
    return $node->id() ?? 0;
  }

  /**
   * Import legacy comments to programs as show_note.
   */
  private function importComment(array $options): void {

    // Limit the number of items to import.
    $limit = intval($options['limit'] ?? 0);
    if ($limit === 0) {
      $limit = $this->getSettings('content_import_limit') ?? 500;
    }

    // Get comments to import.
    $comments = $this->getComments();

    $count = 0;

    foreach ($comments as $item) {

      if ($this->checkExisting($item->timestamp)) {
        continue;
      }

      $program = $this->playlistImport->checkProgram($item->nid);
      if (empty($program)) {
        // I did not log errors here, because we are skipping comments
        // that were attached to news items or other content.
        continue;
      }

      $values = [
        'type' => 'show_note',
        'created' => $item->timestamp,
        'changed' => $item->timestamp,
        'revisions' => 1,
        'status' => 1,
        'language' => Language::LANGCODE_NOT_SPECIFIED,
        'uid' => $this->getSettings('default_owner'),
      ];

      $node = $this->entityTypeManager->getStorage('node')->create($values);
      $node->title->setValue($item->subject);
      $node->body->setValue([
        'value' => $item->comment,
        'format' => 'basic_html',
      ]);
      $node->field_program->target_id = $program;
      $node->save();
      $count++;

      if ($count >= $limit) {
        break;
      }
    }

    $this->messenger()->addMessage($this->t(
      'Imported @count items.',
      ['@count' => $count]
    ));

  }

  /**
   * Get import settings.
   *
   * @param string $key
   *   The settings key to retrieve.
   */
  private function getSettings(string $key) {
    $settings = $this->configFactory->get(static::IMPORT_SETTINGS);
    return $settings->get($key) ?? NULL;
  }

  /**
   * Get index of content to import.
   */
  private function getIndex(string $type): array {

    $url = $this->getSettings('source_uri');
    $endpoint = "$url/$type/index";

    $response = $this->httpClient->request('GET', $endpoint, [RequestOptions::TIMEOUT => 60]);
    if ($response->getStatusCode() == 200) {
      $body = $response->getBody();
      return json_decode($body);
    }
    else {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return [];
    }
  }

  /**
   * Get comments to import.
   */
  private function getComments(): array {
    $url = $this->getSettings('source_uri');
    $endpoint = "$url/comments";

    $response = $this->httpClient->request('GET', $endpoint, [RequestOptions::TIMEOUT => 60]);
    if ($response->getStatusCode() == 200) {
      $body = $response->getBody();
      return json_decode($body);
    }
    else {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return [];
    }
  }

  /**
   * Get source content from node ID.
   */
  private function getContent(int $nid): ?object {

    $url = $this->getSettings('source_uri');
    $endpoint = "$url/node/$nid";

    $response = $this->httpClient->request('GET', $endpoint, [RequestOptions::TIMEOUT => 60]);
    if ($response->getStatusCode() == 200) {
      $body = $response->getBody();
      return json_decode($body);
    }
    else {
      $this->getLogger('whfr_content_import')->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $endpoint,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return NULL;
    }
  }

  /**
   * Get a media entity for the specified file.
   */
  private function getMedia($bundle, $source, $target): ?object {

    // Get field name for media entity or fail.
    $types = [
      'audio' => 'field_media_audio_file',
      'image' => 'field_media_image',
    ];

    if (!$fieldname = $types[$bundle] ?? NULL) {
      return NULL;
    }

    // First make sure the file is here.
    if (!$file = $this->getFile($source, $target)) {
      return NULL;
    }

    $storage = $this->entityTypeManager->getStorage('media');

    $existing = $storage->getQuery()
      ->condition($fieldname, $file->id())
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($existing)) {
      $existing = reset($existing);
      return $storage->load($existing);
    }

    $media = $storage->create(['bundle' => $bundle]);
    $media->$fieldname->target_id = $file->id();
    $media->$fieldname->entity = $file;
    $media->save();
    return $media;
  }

  /**
   * Get the specified file.
   */
  private function getFile($source, $target): ?object {

    if (!file_exists("public://{$target}")) {
      mkdir("public://{$target}", 0775, TRUE);
    }

    $target_uri = "public://{$target}/{$source->filename}";

    $storage = $this->entityTypeManager->getStorage('file');

    $existing = $storage->getQuery()
      ->condition('uri', $target_uri)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($existing)) {
      $existing = reset($existing);
      return $storage->load($existing);
    }

    if (!file_exists($target_uri)) {
      $external_file = $this->getSettings('origin') . '/' . $source->filepath;

      $download = system_retrieve_file($external_file, $target_uri, TRUE, FileSystemInterface::EXISTS_REPLACE);

      if (is_object($download)) {
        return $download;
      }
    }

    return NULL;
  }

  /**
   * Check existing content by timestamp.
   *
   * @param int $timestamp
   *   Content created timestamp to check.
   *
   * @return bool
   *   Returns true if content with timestamp is found.
   */
  private function checkExisting(int $timestamp): bool {

    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('created', $timestamp)
      ->accessCheck(FALSE)
      ->execute();

    return !empty($result);
  }

  /**
   * Find an image gallery from legacy ID.
   *
   * @param int $gid
   *   Legacy Gallery ID to check.
   *
   * @return int
   *   Gallery nid, if found.
   */
  private function checkGallery(int $gid): ?int {

    $result = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('field_legacy_nid', $gid)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($result)) {
      return reset($result);
    }
    return NULL;
  }

  /**
   * Get base target node values from the source data.
   */
  private function getBaseValues(object $source): array {
    return [
      'type' => $source->type,
      'created' => $source->created,
      'changed' => $source->changed,
      'revisions' => 1,
      'status' => 1,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->getSettings('default_owner'),
      // 'name' => $this->currentUser->getAccountName(),
    ];
  }

}
