<?php

namespace Drupal\whfr_helper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\whfr_helper\Entity\ScheduleGridDisplay;

/**
 * Defines the Schedule Grid controller.
 */
class ScheduleGridController extends ControllerBase {

  /**
   * View the schedule grid.
   *
   * @todo Inject whfr_helper service and remove redundant code.
   */
  public function view(): array {
    $output = [];

    $storage = $this->entityTypeManager()->getStorage('whfr_program');

    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('inactive', 1, '<>')
      ->execute();

    if (empty($ids)) {
      return [
        '#markup' => $this->t('No active programs found.'),
      ];
    }

    $programs = $storage->loadMultiple($ids);

    // @todo Figure out how to properly use cache contexts here.
    $output[] = [
      '#theme' => 'whfr_schedule_grid',
      '#schedule' => ScheduleGridDisplay::create($programs),
      '#classical24' => $storage->load(1)->toLink(),
      '#cache' => ['max-age' => 300],
    ];
    return $output;
  }

}
