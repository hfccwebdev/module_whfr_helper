<?php

namespace Drupal\whfr_helper;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\whfr_helper\Entity\ScheduleGridDisplay;

/**
 * Defines the WHFR Helper Service.
 */
class WhfrHelperService {

  use StringTranslationTrait;

  /**
   * Module configuration settings name.
   */
  const MODULE_SETTINGS = 'whfr_helper.settings';

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Core\Database\Connection $database
   *   The current database connection.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Connection $database,
    TimeInterface $time,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the upcoming schedule.
   */
  public function getUpcomingSchedule(int $limit = 6): array {
    $output = [];

    if (!$schedule = $this->getSchedule()) {
      return [
        '#markup' => $this->t('No active programs found.'),
      ];
    }

    // Format a date as numeric day of the week.
    $formatWeekday = function ($t) {
      return $this->dateFormatter->format($t, 'custom', 'w');
    };

    // Format a date as a 24-hour time.
    $formatTime = function ($t) {
      return $this->dateFormatter->format($t, 'custom', 'Hi');
    };

    // Format a date in long format with no time.
    $formatPretty = function ($t) {
      return [
        '#type' => 'inline_template',
        '#template' => '<h3 class="whfr-header-yellow-full-width">{{ d }}</h3>',
        '#context' => [
          'd' => $this->dateFormatter->format($t, 'custom', 'l, F j, Y'),
        ],
      ];
    };

    // Format a show for output.
    $formatShow = function ($show) {
      return [
        '#type' => 'inline_template',
        '#template' => '
          <div class="schedule-row {{ show.class }}">
            <span class="showtimes">{{ show.startpretty }} - {{ show.endpretty }}</span>
            <span class="showlink">{{ show.link }}</span>
          </div>
        ',
        '#context' => ['show' => $show],
      ];
    };

    $now = $this->time->getRequestTime();
    $weekday = $formatWeekday($now);
    $time = $formatTime($now);

    $current = $schedule->get('current', "{$weekday},{$time}");
    $current['class'] = 'current';

    $output[] = $formatPretty($now);
    $output[] = $formatShow($current);

    $preday = $weekday;

    for ($i = 1; $i < $limit; $i++) {

      $next = $schedule->get('next', "{$weekday}, {$time}");

      $weekday = $next['weekday'];
      $time = $next['endhours'];

      if ($weekday !== $preday) {
        $now += 86400;
        $output[] = $formatPretty($now);
        $preday = $weekday;
      }

      $next['class'] = 'upcoming';
      $output[] = $formatShow($next);
    }

    $output['#cache']['max-age'] = 0;
    return $output;
  }

  /**
   * Build schedule from an array of programs.
   */
  public function getSchedule(): ?ScheduleGridDisplay {

    $storage = $this->entityTypeManager->getStorage('whfr_program');

    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('inactive', 1, '<>')
      ->execute();

    if (empty($ids)) {
      return NULL;
    }

    $programs = $storage->loadMultiple($ids);

    return ScheduleGridDisplay::create($programs);
  }

}
