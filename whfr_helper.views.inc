<?php

/**
 * @file
 * Contains views information.
 */

/**
 * Implements hook_views_data_alter().
 */
function whfr_helper_views_data_alter(array &$data) {
  $data['whfr_playlist']['playlist_coverart'] = [
    'title' => t('Cover art'),
    'field' => [
      'title' => t('Cover art'),
      'help' => t('Display cover art for this entity.'),
      'id' => 'playlist_coverart',
    ],
  ];
}
